﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    private static GameManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    [SerializeField]
    //These transforms will be used for their positions to check which of the player objects are on screen
    //as well as spawning the enemies at the side of the screen.
    private Transform leftLimit = null, rightLimit = null;

    [SerializeField]
    //This transform will be used to check if a bullet has exceeded the top of the screen.
    private Transform bulletTopLimit = null;

    [SerializeField]
    //These transforms will be used to limit the enemy spawning vertical position range to fine tune the game.
    private Transform enemySpawnTopLimit = null, enemySpawnBottomLimit = null;

    [Space(10)]
    [SerializeField]
    private Text scoreValue = null;

    private int score = 0;

    // Use this for initialization
    void Start()
    {
        _instance = this;
        Application.targetFrameRate = 60;
        StartCoroutine(EnemySpawner());
        UpdateScoreText();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator EnemySpawner()
    {
        while(true)
        {
            yield return new WaitForSeconds(Constants.EnemySpawnRate);
            GameCharacterPool.GetEnemy().Spawn();
        }
    }

    private void UpdateScoreText()
    {
        scoreValue.text = score.ToString();
    }

    public static float GetLeftLimit()
    {
        float result = 0;

        if (instance.leftLimit != null)
        {
            result = instance.leftLimit.position.x;
        }

        return result;
    }

    public static float GetRightLimit()
    {
        float result = 0;

        if (instance.rightLimit != null)
        {
            result = instance.rightLimit.position.x;
        }

        return result;
    }

    public static float GetBulletTopLimit()
    {
        float result = 0;

        if (instance.bulletTopLimit != null)
        {
            result = instance.bulletTopLimit.position.y;
        }

        return result;
    }

    public static float GetEnemySpawnTop()
    {
        float result = 0;

        if (instance.enemySpawnTopLimit != null)
        {
            result = instance.enemySpawnTopLimit.position.y;
        }

        return result;
    }

    public static float GetEnemySpawnBottom()
    {
        float result = 0;

        if (instance.enemySpawnBottomLimit != null)
        {
            result = instance.enemySpawnBottomLimit.position.y;
        }

        return result;
    }

    public static void EnemyHit()
    {
        instance.score += Constants.EnemyHitScore;
        instance.UpdateScoreText();
    }
}
