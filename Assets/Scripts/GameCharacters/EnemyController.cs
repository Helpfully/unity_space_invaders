﻿using UnityEngine;

public class EnemyController : GameCharacterBase
{
    public bool isInUse = false;

    /// <summary>
    /// -1 for Left and 1 for Right
    /// </summary>
    public int Direction = 0;
    public float DecidedSpeed = 0f;

    // Update is called once per frame
    void Update()
    {
        if (isInUse)
        {
            MovementControl();
            OffScreenControl(); 
        }
    }

    private void OffScreenControl()
    {
        float currentX = transform.position.x;
        float xThreshold = 0;
        //If going right...
        if (Direction > 0)
        {
            xThreshold = GameManager.GetRightLimit() + Direction;
            if (currentX > xThreshold)
            {
                Dispose();
            }
        }
        //If going left...
        else if (Direction < 0)
        {
            xThreshold = GameManager.GetLeftLimit() + Direction;
            if (currentX < xThreshold)
            {
                Dispose();
            }
        }
    }

    private void MovementControl()
    {
        transform.position += Vector3.right * Direction * DecidedSpeed * Time.deltaTime;
    }

    public void Spawn()
    {
        isInUse = true;
        bool isLeft = Random.Range(1, 3) == 1;
        float x = 0;
        float y = 0;

        if (isLeft)
        {
            Direction = -1;
            x = GameManager.GetRightLimit() - Direction;
        }
        else
        {
            Direction = 1;
            x = GameManager.GetLeftLimit() - Direction;
        }

        y = Random.Range(GameManager.GetEnemySpawnBottom(), GameManager.GetEnemySpawnTop());

        DecidedSpeed = Random.Range(Constants.EnemyMovementSpeedMin, Constants.EnemyMovementSpeedMax);
        transform.position = new Vector3(x, y, 0);
    }

    public void Dispose()
    {
        isInUse = false;
        transform.position = GameCharacterPool.GetPoolPosition();
    }
}
