﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(SpriteRenderer))]
public class GameCharacterBase : MonoBehaviour
{
    public float top, bottom, left, right;

    public void Initialize()
    {
        top = GetComponent<SpriteRenderer>().size.y / 2f;
        bottom = -top;
        right = GetComponent<SpriteRenderer>().size.x / 2f;
        left = -right;
    }

    public bool IsCollidingWith(GameCharacterBase other)
    {
        float top1 = transform.position.y + top;
        float bottom1 = transform.position.y + bottom;
        float right1 = transform.position.x + right;
        float left1 = transform.position.x + left;

        float top2 = other.transform.position.y + other.top;
        float bottom2 = other.transform.position.y + other.bottom;
        float right2 = other.transform.position.x + other.right;
        float left2 = other.transform.position.x + other.left;

        bool isTooFarRight = right1 <= left2;
        bool isTooFarLeft = left1 >= right2;
        bool isTooFarTop = top1 <= bottom2;
        bool isTooFarBottom = bottom1 >= top2;

        //If none of these conditions apply, then the two objects are colliding.
        bool result = !(isTooFarTop || isTooFarBottom || isTooFarLeft || isTooFarRight);
        
        return result;
    }
}
