﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : GameCharacterBase
{
    public bool isInUse = false;

    // Update is called once per frame
    void Update()
    {
        if (isInUse)
        {
            MovementControl();
            OffScreenControl(); 
        }
    }

    private void OffScreenControl()
    {
        if (transform.position.y > GameManager.GetBulletTopLimit())
        {
            Dispose();
        }
    }

    private void MovementControl()
    {
        transform.position += Vector3.up * Constants.PlayerBulletSpeed * Time.deltaTime;
    }

    public void CollidedWith(EnemyController enemy)
    {
        GameManager.EnemyHit();
        Dispose();
        enemy.Dispose();
    }

    public void FireAtPosition(Vector3 pos)
    {
        transform.position = pos;
        isInUse = true;
    }

    private void Dispose()
    {
        isInUse = false;
        transform.position = GameCharacterPool.GetPoolPosition();
    }
}
