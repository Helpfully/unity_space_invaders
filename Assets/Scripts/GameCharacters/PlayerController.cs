﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : GameCharacterBase
{
    [SerializeField]
    private Transform firingPoint = null;

    private bool isActive = false;

    // Use this for initialization
    void Start()
    {
        Initialize();
        StartCoroutine(FiringControls());
    }

    // Update is called once per frame
    void Update()
    {
        MovementControls();
        ActiveCheck();
    }

    private void ActiveCheck()
    {
        float x = transform.position.x;
        isActive = x < GameManager.GetRightLimit() && x > GameManager.GetLeftLimit();
    }

    private IEnumerator FiringControls()
    {
        while(true)
        {
            if (isActive)
            {
                GameCharacterPool.GetBullet().FireAtPosition(firingPoint.position);
            }

            yield return new WaitForSeconds(Constants.PlayerFiringRate);
        }
    }

    private void MovementControls()
    {
#if UNITY_IOS && !UNITY_EDITOR
        float horizontalInput = 0;
        if (Input.touchCount > 0)
        {
            Touch firstTouch = Input.GetTouch(0);
            float touchXRatio = firstTouch.position.x / Screen.width;

            if (touchXRatio < 0.5f)
            {
                horizontalInput = -1;
            }
            else
            {
                horizontalInput = 1;
            }
        }
#else
        float horizontalInput = Input.GetAxisRaw("Horizontal");
#endif
        transform.localPosition += Vector3.right * horizontalInput * Constants.PlayerMovementSpeed * Time.deltaTime;

        if (transform.localPosition.x > Constants.MirrorImageTeleportThreshold)
        {
            transform.localPosition += Vector3.left * Constants.MirrorImageTeleportThreshold * 2;
        }
        else if (transform.localPosition.x < -Constants.MirrorImageTeleportThreshold)
        {
            transform.localPosition += Vector3.right * Constants.MirrorImageTeleportThreshold * 2;
        }
    }
}
