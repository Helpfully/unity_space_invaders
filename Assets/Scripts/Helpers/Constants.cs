﻿
public static class Constants
{
    public const float PlayerMovementSpeed = 10.0f;
    public const float PlayerFiringRate = 0.25f;
    public const float PlayerBulletSpeed = 10.0f;

    public const float MirrorImageTeleportThreshold = 32f;

    public const int BulletPoolSize = 20;
    public const int EnemyPoolSize = 20;

    public const float EnemySpawnRate = 1.0f;
    public const float EnemyMovementSpeedMin = 2.5f;
    public const float EnemyMovementSpeedMax = 5.0f;

    public const int EnemyHitScore = 10;

}
