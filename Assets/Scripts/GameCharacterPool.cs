﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCharacterPool : MonoBehaviour
{
    private static GameCharacterPool _instance;
    private static GameCharacterPool instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameCharacterPool>();
            }
            return _instance;
        }
    }

    [SerializeField]
    private Transform BulletPoolParent = null, EnemyPoolParent = null;

    [SerializeField]
    private BulletController bulletTemplate = null;
    [SerializeField]
    private EnemyController enemyTemplate = null;

    [SerializeField]
    private List<BulletController> bulletPool;
    [SerializeField]
    private List<EnemyController> enemyPool;

    private int bulletPoolIndex = 0;
    private int enemyPoolIndex = 0;

    // Use this for initialization
    void Start()
    {
        bulletPool = new List<BulletController>();
        enemyPool = new List<EnemyController>();

        ExtendBulletPool();
        ExtendEnemyPool();

        _instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        CollisionDetection();
    }

    private void CollisionDetection()
    {
        foreach (var bullet in bulletPool)
        {
            if (bullet.isInUse)
            {
                CheckBulletForCollision(bullet);
            }
        }
    }

    private void CheckBulletForCollision(BulletController bullet)
    {
        foreach (var enemy in enemyPool)
        {
            if (enemy.isInUse && bullet.IsCollidingWith(enemy))
            {
                bullet.CollidedWith(enemy);
                break;
            }
        }
    }

    private void ExtendEnemyPool()
    {
        for (int i = 0; i < Constants.EnemyPoolSize; i++)
        {
            var enemy = Instantiate(enemyTemplate);
            enemy.Initialize();
            enemy.transform.SetParent(EnemyPoolParent);
            enemyPool.Add(enemy);
        }
    }

    private void ExtendBulletPool()
    {
        for (int i = 0; i < Constants.BulletPoolSize; i++)
        {
            var bullet = Instantiate(bulletTemplate);
            bullet.Initialize();
            bullet.transform.SetParent(BulletPoolParent);
            bulletPool.Add(bullet);
        }
    }

    private EnemyController GetNextAvailableEnemy()
    {
        EnemyController result = null;

        for (int i = 0; i < enemyPool.Count; i++)
        {
            //This part is for making a looping indexing throughout the pool, 
            //making it faster to find the next available element since we first check the least used ones first.
            int currentIndex = i + enemyPoolIndex + 1;
            currentIndex -= (currentIndex >= enemyPool.Count) ? enemyPool.Count : 0;

            EnemyController enemy = enemyPool[currentIndex];
            if (!enemy.isInUse)
            {
                result = enemy;
                enemyPoolIndex = currentIndex;
                break;
            }
        }

        //If there are no elements available...
        if (result == null)
        {
            int currentIndex = enemyPool.Count;
            ExtendEnemyPool();

            result = enemyPool[currentIndex];
            enemyPoolIndex = currentIndex;
        }

        return result;
    }

    private BulletController GetNextAvailableBullet()
    {
        BulletController result = null;
        

        for (int i = 0; i < bulletPool.Count; i++)
        {
            //This part is for making a looping indexing throughout the pool, 
            //making it faster to find the next available element since we first check the least used ones first.
            int currentIndex = i + bulletPoolIndex + 1;
            currentIndex -= (currentIndex >= bulletPool.Count) ? bulletPool.Count : 0;

            BulletController bullet = bulletPool[currentIndex];
            if (!bullet.isInUse)
            {
                result = bullet;
                bulletPoolIndex = currentIndex;
                break;
            }
        }

        //If there are no elements available...
        if (result == null)
        {
            int currentIndex = bulletPool.Count;
            ExtendBulletPool();

            result = bulletPool[currentIndex];
            bulletPoolIndex = currentIndex;
        }

        return result;
    }

    public static BulletController GetBullet()
    {
        return instance.GetNextAvailableBullet();
    }

    public static EnemyController GetEnemy()
    {
        return instance.GetNextAvailableEnemy();
    }
    
    public static Vector3 GetPoolPosition()
    {
        return instance.transform.position;
    }
}
